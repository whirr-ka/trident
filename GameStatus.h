#pragma once
enum class GameStatus
{
	starting,
	running,
	paused,
	quitting
};