#include "parser.h"
#include "room.h"
#include "gamemap.h"
#include "player.h"
#include <iostream>

using namespace std;

int main()
{

	//Room creation process
	Room* supportOffice = new Room;
	//Set the name for the Room
	supportOffice->setName("ATMECS Support Floor");
	//Set description for the Room
	supportOffice->setDescription("Floor where employees take calls");
	

	Room* breakRoom = new Room;
	breakRoom->setName("ATMECS Break Room");
	breakRoom->setDescription("Room where people eat and chill");
	

	Room* motelSix = new Room;
	motelSix->setName("Crack House Motel 6");
	motelSix->setDescription("Crack House where sketchy people live");
	

	Room* parkingLot = new Room;
	parkingLot->setName("Parking Lot");
	parkingLot->setDescription("ATMECS employees parking lot");

	//Empty room used to map the directions in which a room doesn't have an exit'
	Room* empty = new Room;
    empty->setName("empty");

	//Game Map for the Game. All the connections between rooms are created in this object that uses the map element to associate a room with another using a keyword(north, south, west or east)
	Map* gm = new Map;

	//Structure to create the connections between rooms. setExit(room to set an exit from, room to connect to, direction in which the second room connects from the first one)
	gm->setExit(supportOffice, breakRoom, "west");
	//If a room is not going to have a connection to other room in a certain direction, add a setExit with empty as the second room in the direction in which you wont be able to go to
	gm->setExit(supportOffice, empty, "east");
	gm->setExit(supportOffice, empty, "north");
	gm->setExit(supportOffice, empty, "south");
	gm->setExit(motelSix, parkingLot, "east");
	gm->setExit(motelSix, empty, "west");
	gm->setExit(motelSix, empty, "north");
	gm->setExit(motelSix, empty, "south");
	gm->setExit(parkingLot, breakRoom, "south");
	gm->setExit(parkingLot, motelSix, "west");
	gm->setExit(parkingLot, empty, "east");
	gm->setExit(parkingLot, empty, "north");
	gm->setExit(breakRoom, supportOffice, "east");
	gm->setExit(breakRoom, parkingLot, "north");
	gm->setExit(breakRoom, empty, "south");
	gm->setExit(breakRoom, empty, "west");

	//Player creation process
	Player* p1 = new Player;
	//Set the room in which the player starts from
	p1->setCurrentContainer(supportOffice);
	p1->setHp(100);
	p1->setAttackPower(5);
	p1->setEquipped(NULL);

	//Weapon creation process
	Weapon* phone = new Weapon;
	phone->setLongName("Telephone");
	phone->setStrength(5);
	//Type 1 stands for weapons
	phone->setType(1);
	supportOffice->addToContainer(phone);

	//Monster creation process
	Monster* lenMonster = new Monster;
	lenMonster->setLongName("Len");
	lenMonster->setHp(200);
	//Type 2 stands for monsters
    lenMonster->setType(2);
	//Adds the monster to an specific room
    breakRoom->addToContainer(lenMonster);

	//TODO: Create 10 more rooms and set up the map as some kind of maze

	//TODO: Create 5 more weapons and add them in random rooms in the map

	//TODO: Create 2 more monsters and add them to a random room in the map

	Parser p;
	cout << "Welcome to ATMECS!" << endl;
	cout << "What is your name?" << endl;
	cout << "->";
	string name;
	cin >> name;
	p1->setLongName(name);
	cout << endl;
    cout << "Okay " << p1->getLongName() << ", time to get things done around here. Get to work!" << endl;
    cout << endl;
    cout << "You are in: " << supportOffice->getName() << endl;
    cout << "Type 'help' for commands" << endl;
    cout << endl;
    while(p.readInput(p1, gm));

	return 0;

}
