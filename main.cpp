/*
// License //
Permission to use, copy, modify, and/or distribute this software for any 
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES 
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF 
MERCHANTABILITY AND FITNESS.IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY 
SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES 
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN 
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR 
IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
// End of License //
*/
#include <iostream>
#include <string>
#include <vector>
#include <chrono>
#include <time.h>
#include <random>

using namespace std;

// This is purposely a bare-bones area.
// Go to "Game.h" and  "Game.cpp".
// Particularly, head to "Game.cpp" and find the "start()" function.

int main()
{
	// Creates a Game instance
	Game currentGame();

	// Starts the game
	currentGame.start();

	return 0;
}