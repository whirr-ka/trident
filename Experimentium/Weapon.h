#ifndef WEAPON_H
#define WEAPON_H

#include "Thing.h"

class Weapon : public Thing
{
public:
	int getStrength();
	void setStrength(int);

private:
	int strength;
};

#endif