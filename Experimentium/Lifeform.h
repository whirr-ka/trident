#ifndef LIFEFORM_H
#define LIFEFORM_H

#include "Container.h"
#include "Map.h"
#include "Weapon.h"

using std::string;

class LifeForm : public Thing
{
private:

	bool alive;
    int hp;
    int attackPower;
    Container* currentContainer;
    Container* inventory = new Container;
	Weapon* equipped;
    
public:

	int  getHp();
	void setHp(int);

	Container* getCurrentContainer();
    void	   setCurrentContainer(Container*);

	int  getAttackPower();
	void setAttackPower(int);
    
    Container* getInventory();

    void pickup(Thing*);
    void move(string&, Map&);

    void attack(LifeForm*);

    Weapon* getEquipped();
    void	setEquipped(Weapon*);

	void equip(Weapon*);
	void unequip(Weapon*);
};

#endif