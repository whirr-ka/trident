#include "Container.h"
#include "Lifeform.h"

using std::string;

void LifeForm::setCurrentContainer(Container* newContainer)
{
    currentContainer = newContainer;
	
}

Container* LifeForm::getCurrentContainer()
{
    return currentContainer;
}

void LifeForm::move(std::string& direction, Map& gameMap)
{
    currentContainer = gameMap.getMap()[currentContainer][direction];        
}

Container* LifeForm::getInventory()
{
    return inventory;
}

void LifeForm::pickup(Thing* newThing)
{
    getInventory()->addToContainer(newThing);    
}

int LifeForm::getHp()
{
    return hp;
}

void LifeForm::setHp(int newHealth)
{
    hp = newHealth;
}

int LifeForm::getAttackPower()
{
    return attackPower;
}

void LifeForm::setAttackPower(int newAttackPower)
{
    attackPower = newAttackPower;
}

void LifeForm::attack(LifeForm* targetLifeform)
{
    targetLifeform->setHp(targetLifeform->getHp() - this->getAttackPower());
}

void LifeForm::equip(Weapon* weaponToEquip)
{
    for(unsigned int i = 0; i < inventory->getThingsInContainer().size(); i++)
	{
        if(weaponToEquip->getLongName() == inventory->getThingsInContainer().at(i)->getLongName())
		{
            attackPower = attackPower + weaponToEquip->getStrength();
            equipped = (Weapon*)inventory->getThingsInContainer().at(i);
        }
    }
}

void LifeForm::unequip(Weapon* weaponToUnequip)
{
    for(unsigned int i = 0; i < inventory->getThingsInContainer().size(); i++)
	{
        if(weaponToUnequip->getLongName() == inventory->getThingsInContainer().at(i)->getLongName())
		{
            attackPower = 10;
            equipped = NULL;
        }
    }
}

Weapon* LifeForm::getEquipped()
{
    return equipped;
}

void LifeForm::setEquipped(Weapon* newWeapon)
{
    equipped = newWeapon;    
}