#ifndef GAME_H
#define GAME_H

#include "GameStatus.h"

class Game
{

public:

	Game();
	~Game();

	void init();
	void start();

private:

	bool isInitialized;
	GameStatus gameStatus;

};

#endif