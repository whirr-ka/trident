#include "Game.h"
#include "Parser.h"
#include "Room.h"
#include "Map.h"
#include "Player.h"
#include <iostream>
#include <list>

using namespace std;

Game::Game()
{
	this->init();
}

Game::~Game()
{

}

void Game::init()
{
	isInitialized = true;
}

void Game::start()
{
	// Creating rooms
	Room* supportOffice = new Room;
	supportOffice->setName("ATMECS Support Floor");
	supportOffice->setDescription("Floor where employees take calls");


	Room* breakRoom = new Room;
	breakRoom->setName("ATMECS Break Room");
	breakRoom->setDescription("Room where people eat and chill");


	Room* motelSix = new Room;
	motelSix->setName("Crack House Motel 6");
	motelSix->setDescription("Crack House where sketchy people live");


	Room* parkingLot = new Room;
	parkingLot->setName("Parking Lot");
	parkingLot->setDescription("ATMECS employees parking lot");

	Room* empty = new Room;
	empty->setName("empty");

	//Creating map
	Map* gameMap = new Map;

	// Setting room layout
	gameMap->setExit(supportOffice, breakRoom, "west");
	gameMap->setExit(supportOffice, empty, "east");
	gameMap->setExit(supportOffice, empty, "north");
	gameMap->setExit(supportOffice, empty, "south");
	gameMap->setExit(motelSix, parkingLot, "east");
	gameMap->setExit(motelSix, empty, "west");
	gameMap->setExit(motelSix, empty, "north");
	gameMap->setExit(motelSix, empty, "south");
	gameMap->setExit(parkingLot, breakRoom, "south");
	gameMap->setExit(parkingLot, motelSix, "west");
	gameMap->setExit(parkingLot, empty, "east");
	gameMap->setExit(parkingLot, empty, "north");
	gameMap->setExit(breakRoom, supportOffice, "east");
	gameMap->setExit(breakRoom, parkingLot, "north");
	gameMap->setExit(breakRoom, empty, "south");
	gameMap->setExit(breakRoom, empty, "west");

	//Creating player
	Player* player = new Player;
	player->setCurrentContainer(supportOffice);
	player->setHp(100);
	player->setAttackPower(5);
	player->setEquipped(NULL);

	//Creating weapons
	Weapon* phone = new Weapon;
	phone->setLongName("Telephone");
	phone->setStrength(5);
	phone->setType(1);
	supportOffice->addToContainer(phone);

	//Creating monsters
	Monster* lenMonster = new Monster;
	lenMonster->setLongName("Len the Manager");
	lenMonster->setHp(200);
	lenMonster->setType(2);
	breakRoom->addToContainer(lenMonster);


	//Setting up parser
	Parser p;

	//Intro + name get
	cout << "Welcome to ATMECS!" << endl;
	cout << "What is your name?" << endl;
	cout << "->";
	string name;
	cin >> name;
	player->setLongName(name);
	cout << endl;

	// Intro 2
	cout << "Okay " << player->getLongName() << ", time to get things done around here. Get to work!" << endl;
	cout << endl;
	cout << "You are in: " << supportOffice->getName() << endl;
	cout << "Type 'help' for commands" << endl;
	cout << endl;

	// THE GAME LOOP
	while (p.readInput(player, gameMap));
}
//It is a pointer, you are not calling the object directly, you create a reference to it instead
