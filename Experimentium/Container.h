#ifndef CONTAINER_H
#define CONTAINER_H

#include "Thing.h"
#include <vector>

using std::string;
using std::vector;

class Container
{

public:

	Container();

	// Basic info
	string	getName();
	void	setName(string);
    string	getDescription();
    void	setDescription(string);
    
	// Get everything from the contrainer
    vector<Thing*> getThingsInContainer();

	// Add a single item to the container
	void addToContainer(Thing*);
    void removeFromContainer(Thing*);

private:

	string name;
	string description;

	vector<Thing*> thingsInContainer;
	
};

#endif