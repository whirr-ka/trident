#include "Parser.h"
#include <iostream>

using std::string;
using std::cout;
using std::cin;
using std::endl;

bool Parser::readInput(Player* player, Map* gameMap)
{
	string verb;

	// Get player input
    cout << "-> ";    
    cin >> verb;

	// Decide based on first input word
    if(verb == "help")
	{
        cout << "Commands that you can use:" << endl;
        cout << " -stats(Shows your player stats)" << endl;
        cout << " -look (Describes what is around you)" << endl;
        cout << " -go [dir] (north, east, south, west)" << endl;
        cout << " -inventory (Check what are you carrying)" << endl;
        cout << " -get [object name](Pick up objects)" << endl;
        cout << " -drop [object name] (Drop something in your inventory)" << endl;
        cout << " -equip [object name](Equip weapons to improve your power)" << endl;
        cout << " -unequip [object name] (Unequip weapons)" << endl;
        cout << " -equiped (Check what you have equiped)" << endl;
        cout << " -attack [target] (Attack the monsters)" << endl;
        cout << " -quit" << endl;
        cout << endl;
    }
	else if(verb == "quit")
	{
        return false;
    }
	else if(verb == "stats")
	{
        cout << "Player Name: " << player->getLongName() << endl;
        cout << "Stats: " << endl;
        cout << " -Health Points: " << player->getHp() << endl;
        cout << " -Attack Power: " << player->getAttackPower() << endl; 
        cout << endl;
    }
	else if(verb == "inventory")
	{
        if(player->getInventory()->getThingsInContainer().empty())
		{
            cout << "Your inventory seems to be empty" << endl;
            cout << endl;
        }
		else
		{
            cout << "Inventory: " << endl;
            for(unsigned int i = 0; i < player->getInventory()->getThingsInContainer().size(); i++)
            {
                cout << "- "<< player->getInventory()->getThingsInContainer().at(i)->getLongName() << endl;
            }
            cout << endl;
        }   
    }
	else if(verb == "go")
	{
        string direction;
        cin >> direction;
        if(direction == "north" || direction == "west" || direction == "south" || direction == "east")
		{
            Container* currentroom = new Container;
            currentroom = player->getCurrentContainer();
            player->move(direction, *gameMap);
            if(player->getCurrentContainer()->getName() == "empty")
			{
                player->setCurrentContainer(currentroom);
                cout << "There is nothing that way" << endl;
                cout << endl;
            }
			else
			{
                cout << "You are now in " << player->getCurrentContainer()->getName() << endl;
                cout << endl;
            }
        }
		else
		{
            cout << "Seems like you typed a wrong direction" << endl;
            cout << "Type 'help' for commands" << endl;
            cout << endl;
        }
    }
	else if(verb == "look")
	{
        cout << "In " << player->getCurrentContainer()->getName() << endl;
        cout << "    " << player->getCurrentContainer()->getDescription() << endl;

        if(player->getCurrentContainer()->getThingsInContainer().empty())
        {
            cout << "Room seems to be empty. " << endl;
            cout << endl;
        }
		else
		{
			// Display list of things in room
            cout << "Things in this room: " << endl;
            for(unsigned int i = 0; i < player->getCurrentContainer()->getThingsInContainer().size(); i++)
            {
                if(player->getCurrentContainer()->getThingsInContainer().at(i)->getType() == 1)
				{
					cout << "- "<< player->getCurrentContainer()->getThingsInContainer().at(i)->getLongName() << endl;
                }
            }
            cout << endl;

			// Display list of monsters in room
            cout << "Monsters in this room: " << endl;
            for(unsigned int i = 0; i < player->getCurrentContainer()->getThingsInContainer().size(); i++)
            {
                if(player->getCurrentContainer()->getThingsInContainer().at(i)->getType() == 2)
				{
					cout << "- "<< player->getCurrentContainer()->getThingsInContainer().at(i)->getLongName() << endl;
                }

            }
            cout << endl; 
        }
    }
	else if(verb == "get")
	{
        string object;
        cin >> object;
        bool in = false;
        if(player->getCurrentContainer()->getThingsInContainer().empty())
		{
            cout << "There is no " << object << " in this room" << endl;
            cout << endl;
        } else {
            for(unsigned int i = 0; i < player->getCurrentContainer()->getThingsInContainer().size(); i++)
            {
                if(player->getCurrentContainer()->getThingsInContainer().at(i)->getLongName() == object)
                {
                    cout << "You picked up a " << object << endl;
                    player->pickup((*player).getCurrentContainer()->getThingsInContainer().at(i));
                    player->getCurrentContainer()->removeFromContainer(player->getCurrentContainer()->getThingsInContainer().at(i));
                    cout << endl;
                    in = true;
                }
            }
            if(!in)
			{
                cout << "You don't have a "<< object << " in your inventory." << endl;
            }   
        }
    }
	// "drop" X
	else if(verb == "drop")
	{
		
        string thingName;
        cin >> thingName;

        bool in = false;

		// If inventory is empty
        if(player->getInventory()->getThingsInContainer().empty())
		{
            cout << "There is no " << thingName << " in your inventory" << endl;
            cout << endl;
        }
		// Else contains items
		else 
		{
            for(unsigned int i = 0; i < player->getInventory()->getThingsInContainer().size(); i++)
            {
                if(player->getInventory()->getThingsInContainer().at(i)->getLongName() == thingName)
				{
                    if(player->getEquipped() != NULL && player->getEquipped()->getLongName() == thingName)
					{
                        cout << "Unequip object first!" << endl;
                        cout << endl;
                    }
					else
					{
                        cout << "You dropped a " << thingName << endl;
                        player->getCurrentContainer()->addToContainer(player->getInventory()->getThingsInContainer().at(i));
                        player->getInventory()->removeFromContainer(player->getInventory()->getThingsInContainer().at(i));
                        cout << endl;
                    }
                }
                in = true;
            }
            if(!in)
			{
                cout << "You don't have a "<< thingName << " in your inventory." << endl;
            }   
        }
    }
	else if (verb == "equip")
	{
        string w;
        cin >> w;
        bool in = false;
        if(player->getInventory()->getThingsInContainer().empty())
		{
            cout << "Seems like your inventory is empty..." << endl;
            cout << endl;
        }
		else
		{
            for(unsigned int i = 0; i < player->getInventory()->getThingsInContainer().size(); i++)
			{
                if(player->getInventory()->getThingsInContainer().at(i)->getLongName() == w)
				{
                    if( player->getEquipped() == NULL)
					{
						cout << "You equiped a " << w << endl;
						player->equip((Weapon*)player->getInventory()->getThingsInContainer().at(i));
						cout << endl;
                    }
					else
					{
                        cout << "You already have something equiped" << endl;
                        cout << endl;
                    }
					in = true;
                }
            }
            if(!in)
			{
                cout << "You don't have a "<< w << " in your inventory." << endl;
            }   
        }       
    }
	else if (verb == "unequip")
	{
        string weaponName;
        cin >> weaponName;

        if(player->getInventory()->getThingsInContainer().empty())
		{
            cout << "Seems like you don't have anything..." << endl;
            cout << endl;
        }
		else
		{
            for(unsigned int i = 0; i < player->getInventory()->getThingsInContainer().size(); i++)
			{
                if(player->getInventory()->getThingsInContainer().at(i)->getLongName() == weaponName && player->getEquipped()->getLongName() == weaponName)
				{
                    cout << "Unequipped " << weaponName << endl;
                    player->unequip((Weapon*)player->getInventory()->getThingsInContainer().at(i));
                    cout << endl;
                }
            }   
        }
    }
	else if(verb == "equipped")
	{
        if(player->getEquipped() == NULL)
		{
            cout << "All you got are your hands...\n" << endl;
        }
		else
		{
            cout << "You have a " << player->getEquipped()->getLongName() << " equipped.\n" << endl;
        }
    }
	else if (verb == "attack")
	{
        string targetToAttack;
        cin >> targetToAttack;

        bool targetExists = false;

		// No attack target
        if(player->getCurrentContainer()->getThingsInContainer().empty()) //Problem? Current container, is that the room?
		{
            cout << "Nothing to attack here... You are seeing things..." << endl;
            cout << endl;
        }
		else
		{
            for (unsigned int i = 0; i < player->getCurrentContainer()->getThingsInContainer().size(); i++)
			{ 
                if(player->getCurrentContainer()->getThingsInContainer().at(i)->getLongName() == targetToAttack)
				{
                    cout << "You attacked " << targetToAttack << endl;
                    player->attack((LifeForm*)player->getCurrentContainer()->getThingsInContainer().at(i));
					// If target is alive
                    if(((Monster*)player->getCurrentContainer()->getThingsInContainer().at(i))->getHp() > 0)
					{
                        cout << targetToAttack << "'s hp is now "
							 << ((Monster*)player->getCurrentContainer()->getThingsInContainer().at(i))->getHp() 
							 << ".\n" << endl;
                    }
					else // Else target is dead
					{
                        cout << targetToAttack << " is now dead.\n" << endl;

                        player->getCurrentContainer()->removeFromContainer(player->getCurrentContainer()->getThingsInContainer().at(i));
                    }
					targetExists = true;
                } 
            }

			// No target
            if(!targetExists)
			{
                cout << "There is no " << targetToAttack << " in this room." << endl;
            }
        }
    }
	else
	{
        cout << "UNKNOWN COMMAND, type 'help'" << endl;
    }
    return true;
}