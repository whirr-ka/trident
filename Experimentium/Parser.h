#ifndef PARSER_H
#define PARSER_H

#include "Map.h"
#include "Player.h"
//#include "Container.h"
//#include "Monster.h"

class Parser
{
public:
    bool readInput(Player*, Map*);
};

#endif