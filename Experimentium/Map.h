#ifndef GAMEMAP_H
#define GAMEMAP_H

#include "Container.h"

#include <map>
#include <utility>
#include <string>

using std::map;
using std::string;

class Map 
{
private:

    map<Container*, map<string, Container*> > exits;

public:

	void setExit(Container* currentRoom, Container* adjacentRoom, string dir);

	map<Container*, map<string, Container*> > getMap();
};

#endif