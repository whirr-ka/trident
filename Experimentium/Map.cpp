#include "Map.h"

void Map::setExit(Container* currentRoom, Container* adjacentRoom, string direction)
{
	exits[currentRoom][direction] = adjacentRoom;
}

map<Container*, map<string, Container*> > Map::getMap()
{
	return exits;
}