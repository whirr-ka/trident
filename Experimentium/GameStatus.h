#ifndef GAMESTATUS_H
#define GAMESTATUS_H

enum class GameStatus
{
	starting,
	running,
	paused,
	quitting
};

#endif