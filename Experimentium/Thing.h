#ifndef THING_H
#define THING_H

#include <vector>
#include <string>

using std::string;

class Thing
{
public:
	string getLongName();
	void setLongName(string);

	string getDescription();
	void setDescription(string);

	int getType();
	void setType(int);

private:
	int type;
    string longName;
    string description;
};

#endif