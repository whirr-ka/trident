#include "Container.h"
#include <iostream>

using std::vector;
using std::string;
using std::swap;

// Constructor
Container::Container()
{

}

// Name
string Container::getName()
{
	return name;
}
void Container::setName(string newName)
{
	name = newName;
}

// Description
string Container::getDescription()
{
	return description;
}
void Container::setDescription(string newDescription)
{
    description = newDescription;
}

// Container
vector<Thing*> Container::getThingsInContainer()
{
   return thingsInContainer;
}
void Container::addToContainer(Thing* t)
{
    thingsInContainer.push_back(t);
}
void Container::removeFromContainer(Thing* thing)
{
    for(unsigned int i = 0; i < thingsInContainer.size(); i++)
    {
        if(thing->getLongName() == thingsInContainer.at(i)->getLongName())
        {
			// TODO: Check for performance
            swap(thingsInContainer.at(i), thingsInContainer.back());
            thingsInContainer.pop_back();
        }
    }
}